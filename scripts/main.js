let url;
let showmenu = false;


function init(){
    showmenu = false;
    if(showmenu){
        document.getElementById("menu").style.display = 'inline-block';
        document.getElementById("mainview").style.gridColumn = "span 1";
        document.getElementById("content").style.overflowX = "hidden";
        document.body.style.overflow = "hidden";
    }
    else{
        document.getElementById("menu").style.display = 'none';
        document.getElementById("mainview").style.gridColumn = "span 2";
    }
    url = new URL(window.location.href);
    hash = url.hash.slice(1);
    if(hash==""){
        loadblog('home');
    }
    else{
        loadblog(hash);
    }
    window.scrollTo(0,1);
}

function togglemenu(){
    if(showmenu){
        document.getElementById("menu").style.display = 'none';
        document.getElementById("mainview").style.gridColumn = "span 2";
        showmenu = false;
    }
    else{
        document.getElementById("menu").style.display = 'inline-block';
        document.getElementById("mainview").style.gridColumn = "span 1";
        document.getElementById("content").style.overflowX = "hidden";
        document.body.style.overflow = "hidden";
        showmenu = true;
    }
}

function loadblog(bloghash){
    fetch('/posts/' + bloghash + '.md').then(function(response) {
        if(response.ok) {
            return response.text()
        }
        throw new Error('Network response was not ok.')
    }).then(function(text) {
        md = new Remarkable('full',{html:true,breaks:true});
        document.getElementById('content').innerHTML = md.render(text);
    }).catch(function(error) {
        console.log('Failed to load: ', error.message)
    });
}
